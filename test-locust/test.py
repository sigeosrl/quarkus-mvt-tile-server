from locust import HttpUser, task, between
import random

class KeycloakUser(HttpUser):
    wait_time = between(1, 2.5)

    def on_start(self):

        url = 'https://KEYCLOAK/realms/YOUR_REALM/protocol/openid-connect/token'
        payload = {
            'client_id': 'tile-server',
            'client_secret': 'YOUR_CLIENT_SECRET',
            'username': 'YOUR_USERNAME',
            'password': 'YOUR_PASSWORD',
            'grant_type': 'password'
        }
        
        with self.client.post(url, data=payload, catch_response=True) as response:
            if response.status_code != 200:
                response.failure("Failed to get token")
            else:
                json_response_dict = response.json()
                self.token = json_response_dict['access_token']

    @task(1)
    def task1(self):
        print("task1 called")

        # Randomly select values for the URL parameters
        tile = random.choice(['test1', 'test4', 'test7'])
        z = random.randint(1, 19)  # Assuming z can also be between 1 and 19
        x = random.randint(0, 500)
        y = random.randint(0, 500)

        # Construct the URL
        url = f"http://tile-server:18765/ts/tiles/{tile}/{z}/{x}/{y}"

        headers = {'Authorization': 'Bearer ' + self.token}
        # Make the GET request
        with self.client.get(url, headers=headers, catch_response=True) as response:
                print(response.status_code)
                print(response.content)
