package com.sigeosrl.tileserver.config;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgisContainerProvider;

import java.util.HashMap;
import java.util.Map;

@QuarkusTestResource(TestContainers.Initializer.class)
public class TestContainers {

    public static class Initializer implements QuarkusTestResourceLifecycleManager {

        private JdbcDatabaseContainer<?> projectPostgreSQLContainer;
        private JdbcDatabaseContainer<?> tenantPostgreSQLContainer;

        @Override
        public Map<String, String> start() {
            try {
                projectPostgreSQLContainer = new PostgisContainerProvider()
                        .newInstance()
                        .withDatabaseName("projectdb")
                        .withUsername("dev")
                        .withPassword("x")
                        .withInitScript("sql/project_init.sql");

                projectPostgreSQLContainer.start();

                tenantPostgreSQLContainer = new PostgisContainerProvider()
                        .newInstance()
                        .withDatabaseName("testDB")
                        .withUsername("dev")
                        .withPassword("x")
                        .withInitScript("sql/test_tenant.sql");

                tenantPostgreSQLContainer.start();

            } catch (Exception e) {

                throw new RuntimeException("Error while starting test containers", e);
            }

            return configurationParameters();
        }

        private Map<String, String> configurationParameters() {
            final Map<String, String> configurationParameters = new HashMap<>();

            //Postgresql configuration parameters
            configurationParameters.put("DB_PROJECT_HOST", projectPostgreSQLContainer.getHost());
            configurationParameters.put("DB_PROJECT_PORT", String.valueOf(projectPostgreSQLContainer.getFirstMappedPort()));
            configurationParameters.put("DB_PROJECT_NAME", projectPostgreSQLContainer.getDatabaseName());
            configurationParameters.put("DB_PROJECT_USER", projectPostgreSQLContainer.getUsername());
            configurationParameters.put("DB_PROJECT_PASS", projectPostgreSQLContainer.getPassword());
            configurationParameters.put("DB_PROJECT_EXTERNAL_PORT", String.valueOf(projectPostgreSQLContainer.getFirstMappedPort()));
            configurationParameters.put("quarkus.datasource.jdbc.url", projectPostgreSQLContainer.getJdbcUrl());
            configurationParameters.put("quarkus.datasource.jdbc.driver", "org.postgresql.Driver?currentSchema=tiles,util");
            configurationParameters.put("quarkus.datasource.db-kind", "postgresql");
            configurationParameters.put("quarkus.hibernate-orm.database.default-schema", "tiles");
            configurationParameters.put("quarkus.hibernate-orm.database.generation", "drop-and-create");
            configurationParameters.put("database.init-file", "src/test/resources/sql/project_data.sql");


            configurationParameters.put("DB_TENANT1_HOST", tenantPostgreSQLContainer.getHost());
            configurationParameters.put("DB_TENANT1_PORT", String.valueOf(tenantPostgreSQLContainer.getFirstMappedPort()));
            configurationParameters.put("DB_TENANT1_NAME", tenantPostgreSQLContainer.getDatabaseName());
            configurationParameters.put("DB_TENANT1_USER", tenantPostgreSQLContainer.getUsername());
            configurationParameters.put("DB_TENANT1_PASS", tenantPostgreSQLContainer.getPassword());
            configurationParameters.put("DB_TENANT1_EXTERNAL_PORT", String.valueOf(tenantPostgreSQLContainer.getFirstMappedPort()));

            return configurationParameters;
        }

        @Override
        public void stop() {
            if (projectPostgreSQLContainer != null && projectPostgreSQLContainer.isRunning()) {
                projectPostgreSQLContainer.stop();
            }
            if (tenantPostgreSQLContainer != null && tenantPostgreSQLContainer.isRunning()) {
                tenantPostgreSQLContainer.stop();
            }
        }
    }
}
