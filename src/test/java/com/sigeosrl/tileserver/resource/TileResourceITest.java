package com.sigeosrl.tileserver.resource;

import com.sigeosrl.tileserver.config.TestContainers;
import com.sigeosrl.tileserver.model.Tenant;
import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.service.TenantService;
import com.sigeosrl.tileserver.util.Util;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(TestContainers.Initializer.class)
class TileResourceITest {

    @Inject
    TenantService tenantService;

    @ConfigProperty(name = "DB_PROJECT_HOST")
    String dbTenant1Host;
    @ConfigProperty(name = "DB_TENANT1_PORT")
    String dbTenant1Port;

    @ConfigProperty(name = "client.for.roles", defaultValue = "client")
    String clientForRoles;

    @Test
    @Order(1)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    @TestHTTPEndpoint(TileResource.class)
    void createTile() {

        String tileSource = "{ \"allowedRoles\": [\n" +
                "    \"ADMIN\",\n" +
                "    \"USER\"\n" +
                "  ],\n" +
                "  \"ctx\": [\n" +
                "    \"CTX2\",\n" +
                "    \"CTX1\"\n" +
                "  ],\n" +
                "  \"description\": \"created\",\n" +
                "  \"features\": \"id, name\",\n" +
                "  \"filter\": \"1 = 1\",\n" +
                "  \"geomColumn\": \"geom\",\n" +
                "  \"name\": \"test10\",\n" +
                "  \"schema\": \"tiles\",\n" +
                "  \"srid\": 4326,\n" +
                "  \"table\": \"geometry4\",\n" +
                "  \"tenant\": {\n" +
                "    \"jdbcurl\": \"jdbc:postgresql://db-tenant1:5432/test-ts\",\n" +
                "    \"name\": \"tenant1\",\n" +
                "    \"password\": \"x\",\n" +
                "    \"username\": \"dev\"\n" +
                "  }}";


        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .contentType(ContentType.JSON)
                .body(tileSource)
                .when()
                .post("")
                .then()
                .statusCode(200)
                .assertThat().body("name", is("test10"))
                .assertThat().body("description", is("created"));
    }

    @Test
    @Order(2)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    @TestHTTPEndpoint(TileResource.class)
    void getTile() {
        String tileName = "test2";

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .get("/{tile}", tileName)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .assertThat().body("name", is("test2"));
    }

    @Test
    @Order(3)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    @TestHTTPEndpoint(TileResource.class)
    void getAllTileSources() {

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .get("")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .assertThat().body("tile_source.size()", is(9));
    }

    @Test
    @Order(4)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    @TestHTTPEndpoint(TileResource.class)
    void updateTileSourceTest() {
        String tileName = "test2";
        Tenant tenant = new Tenant("tenant1", "jdbc:oracle:thin:@db-project1:5432/test-ts", "dev", "x");
        TileSource updatedTileSource = new TileSource()
                .setDescription("updated")
                .setTenant(tenant);


        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .contentType(ContentType.JSON)
                .body(updatedTileSource)
                .when()
                .put("/{tile}", tileName)
                .then()
                .statusCode(200)
                .assertThat().body("description", is("updated"));
    }

    @Test
    @Order(5)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    @TestHTTPEndpoint(TileResource.class)
    void deleteTile() {
        String tile = "test2";
        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .delete("/{tile}", tile)
                .then()
                .statusCode(200);
    }

    @Test
    @Order(6)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void getTileWithParameters() {

        String tile = "test1";
        int z = 10;
        int x = 100;
        int y = 100;

        String tenantName = "tenant1";

        Tenant tenant = tenantService.getTenant(tenantName);
        tenant.setJdbcUrl("jdbc:postgresql://" + dbTenant1Host + ":" + dbTenant1Port + "/testDB");
        tenant.setPassword("x");

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .contentType(ContentType.JSON)
                .body(tenant)
                .when()
                .put("/tenants/{tenant}", tenantName)
                .then()
                .statusCode(200);

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .contentType(ContentType.BINARY)
                .when()
                .get("/tiles/{tile}/{z}/{x}/{y}", tile, z, x, y)
                .then()
                .statusCode(200);
    }

    @Test
    @Order(7)
    void getTileWithParametersAndToken() {

        String tenantName = "tenant1";

        Tenant tenant = tenantService.getTenant(tenantName);
        tenant.setJdbcUrl("jdbc:postgresql://" + dbTenant1Host + ":" + dbTenant1Port + "/testDB");
        tenant.setPassword("x");

        given()
                .contentType(ContentType.JSON)
                .body(tenant)
                .when()
                .put("/tenants/{tenant}", tenantName)
                .then()
                .statusCode(200);

        String body = given()
                .contentType(ContentType.BINARY)
                .when()
                .get("/tiles/regioni/12/2204/1524?srid=32632&token=4d9781d5-c78d-4a08-b262-0c38dc17be82")
                .then()
                .statusCode(200)
                .body(notNullValue()).extract().asString();
        System.out.println("####Tile: " + body);

        given()
                .contentType(ContentType.BINARY)
                .when()
                .get("/tiles/regioni/12/2204/1524?srid=32632")
                .then()
                .statusCode(401);
    }

    @Test
    @Order(8)
//    @TestSecurity(user = "admin", roles = "CFAA-ADMIN")
    void getTileWithParametersWhenNoRolesAsADMIN() {

        String tile = "test3";
        int z = 10;
        int x = 100;
        int y = 100;

        String tenantName = "tenant1";

        Tenant tenant = tenantService.getTenant(tenantName);
        tenant.setJdbcUrl("jdbc:postgresql://" + dbTenant1Host + ":" + dbTenant1Port + "/testDB");
        tenant.setPassword("x");

        given().auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("CFAA-ADMIN")))
                .contentType(ContentType.JSON)
                .body(tenant)
                .when()
                .put("/tenants/{tenant}", tenantName)
                .then()
                .statusCode(200);

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("CFAA-ADMIN")))
                .contentType(ContentType.BINARY)
                .when()
                .get("/tiles/{tile}/{z}/{x}/{y}", tile, z, x, y)
                .then()
                .statusCode(200);
    }

    @Test
    @Order(9)
//    @TestSecurity(user = "user", roles = "CFAA-USER")
    void getTileWithParametersWhenNoRolesAsUSER() {

        String tile = "test3";
        int z = 10;
        int x = 100;
        int y = 100;

        String tenantName = "tenant1";

        Tenant tenant = tenantService.getTenant(tenantName);
        tenant.setJdbcUrl("jdbc:postgresql://" + dbTenant1Host + ":" + dbTenant1Port + "/testDB");
        tenant.setPassword("x");

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("CFAA-USER")))
                .contentType(ContentType.JSON)
                .body(tenant)
                .when()
                .put("/tenants/{tenant}", tenantName)
                .then()
                .statusCode(200);

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("CFAA-USER")))
                .contentType(ContentType.BINARY)
                .when()
                .get("/tiles/{tile}/{z}/{x}/{y}", tile, z, x, y)
                .then()
                .statusCode(200);
    }

    @Test
    @Order(100)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    @TestHTTPEndpoint(TileResource.class)
    void invalidateCacheTest() {
        // Send a POST request to the invalidateCache endpoint
        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .post("/invalidate")
                .then()
                .statusCode(200)
                .body(equalTo("Cache invalidated"));
    }
}