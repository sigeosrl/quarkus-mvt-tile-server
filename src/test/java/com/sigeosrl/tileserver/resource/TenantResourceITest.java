package com.sigeosrl.tileserver.resource;

import com.sigeosrl.tileserver.model.Tenant;
import com.sigeosrl.tileserver.util.Util;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;


@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestHTTPEndpoint(TenantResource.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class TenantResourceITest {

    @ConfigProperty(name = "client.for.roles", defaultValue = "client")
    String clientForRoles;

    @Test
    @Order(1)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void createTenant() {

        Tenant newTenant = new Tenant();
        newTenant.setJdbcUrl("jdbc:postgresql://db-tenant2:5432/test-ts");
        newTenant.setName("tenant2");
        newTenant.setPassword("x");
        newTenant.setUsername("dev");


        given()
                .contentType(ContentType.JSON)
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin",  Set.of("ADMIN")))
                .body(newTenant)
                .when()
                .post("")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(notNullValue())
                .assertThat().body("username", is("dev"));
    }

    @Test
    @Order(2)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void getTenant() {
        String tenant = "tenant2";

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .get("/{tenant}", tenant)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .assertThat().body("username", is("dev"));
    }

    @Test
    @Order(3)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void getAllTenants() {

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .get("")
                .then()
                .statusCode(200);
    }

    @Test
    @Order(4)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void updateTenant() {
        String tenantName = "tenant2";

        Tenant updatedTenant = new Tenant()
                .setJdbcUrl("jdbc:postgresql://db-tenant-updated:5432/test-ts")
                .setPassword("updatedPassword")
                .setUsername("updatedUser");

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .contentType(ContentType.JSON)
                .body(updatedTenant)
                .when()
                .put("/{tenant}", tenantName)
                .then()
                .statusCode(200)
                .assertThat().body("username", is("updatedUser"));
    }

    @Test
    @Order(5)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void deleteTenant() {
        String tenant = "tenant2";
        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .delete("/{tenant}", tenant)
                .then()
                .statusCode(200);
    }
}