package com.sigeosrl.tileserver.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sigeosrl.tileserver.util.Util;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestHTTPEndpoint(TokenResource.class)
class TokenResourceITest {

    @ConfigProperty(name = "client.for.roles", defaultValue = "client")
    String clientForRoles;

    @Test
    @Order(1)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void createToken() throws JsonProcessingException {

        String token = "{\n" +
                "    \"tileSources\": [\n" +
                "    ],\n" +
                "    \"expiration\": \"2025-06-01T10:15:30.123+0100\"\n" +
                "}";

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .contentType(ContentType.JSON)
                .body(token)
                .when()
                .post("")
                .then()
                .statusCode(200)
                .assertThat().body("token", notNullValue())
                .log();
    }

    @Test
    @Order(2)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void getToken() {
        String token = "4d9781d5-c78d-4a08-b262-0c38dc17be82";
        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .get("/{token}", token)
                .then()
                .statusCode(200)
                .assertThat().body("token", notNullValue())
                .assertThat().body("expiration", notNullValue())
                .log();
    }

    @Test
    @Order(3)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void getAllTokens() {

        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .get("")
                .then()
                .statusCode(200)
                .assertThat().body("token.size()", is(2))
                .log();
    }

    @Test
    @Order(4)
//    @TestSecurity(user = "admin", roles = "ADMIN")
    void deleteToken() {
        String token = "4d9781d5-c78d-4a08-b262-0c38dc17be82";
        given()
                .auth().oauth2(Util.getAccessToken(clientForRoles,"client","admin", Set.of("ADMIN")))
                .when()
                .delete("/{token}", token)
                .then()
                .statusCode(200)
                .log();
    }

}
