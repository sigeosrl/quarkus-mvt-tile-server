package com.sigeosrl.tileserver.resource;

import com.sigeosrl.tileserver.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class UtilsUTest {

    @Test
    void encrypt() throws Exception {

        String plainText = "x";
        String encrypted = Utils.encrypt(plainText, "xxxxxxxxxxxxxxxxxxxxxxxx");
        System.out.println(encrypted);
        Assertions.assertEquals("x", Utils.decrypt(encrypted, "xxxxxxxxxxxxxxxxxxxxxxxx"));
    }
}
