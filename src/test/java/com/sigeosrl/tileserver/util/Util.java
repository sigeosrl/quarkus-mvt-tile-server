package com.sigeosrl.tileserver.util;

import io.smallrye.jwt.build.Jwt;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Util {

    public static String getAccessToken(String clientForRoles, String client, String userName, Set<String> roles) {

        if (clientForRoles != null && !clientForRoles.isEmpty()) {

            Map<String, Object> rolesMap = new HashMap<>();
            rolesMap.put("roles", roles);

            Map<String, Object> resourceAccess = new HashMap<>();
            resourceAccess.put(client, rolesMap);

            String a = Jwt
                    .preferredUserName(userName)
                    .claim("resource_access", resourceAccess)
                    .claim("scope", "openid")
                    .issuer("https://server.example.com")
                    .audience("https://service.example.com")
                    .sign();
            System.out.println("@@@JWT " + a);
            return a;

        } else {
            Map<String, Object> rolesMap = new HashMap<>();
            rolesMap.put("roles", roles);

            String a = Jwt
                    .preferredUserName(userName)
                    .claim("realm_access", rolesMap)
                    .claim("scope", "openid")
                    .issuer("https://server.example.com")
                    .audience("https://service.example.com")
                    .sign();
            System.out.println("@@@JWT " + a);
            return a;
        }
    }
}
