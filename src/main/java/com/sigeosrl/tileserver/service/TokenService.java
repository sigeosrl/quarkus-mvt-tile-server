package com.sigeosrl.tileserver.service;

import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.model.Token;
import com.sigeosrl.tileserver.repository.TokenRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.NotFoundException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@ApplicationScoped
public class TokenService {

    private final TokenRepository tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    // token is valid if present and not expired
    public boolean isValid(UUID token) {
        Token t = tokenRepository.findById(token).orElseThrow(() -> new NotFoundException("Token not found"));
        return t.getExpiration().isAfter(ZonedDateTime.now());
    }

    public List<Token> getTokens() {
        return tokenRepository.findAll();
    }

    public Token getToken(UUID token) {
        return tokenRepository.findById(token).orElse(null);
    }

    public Token createToken(Set<TileSource> tileSourceSet, ZonedDateTime expiration) {
        Token token = new Token();
        token.setToken(UUID.randomUUID());
        token.setExpiration(expiration);
        token.setTileSourceSet(tileSourceSet);
        return tokenRepository.save(token);
    }

    public void deleteToken(UUID token) {
        tokenRepository.deleteById(token);
    }
}
