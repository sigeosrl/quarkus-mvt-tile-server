package com.sigeosrl.tileserver.service;

import com.sigeosrl.tileserver.config.TenantDataSourceProvider;
import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.repository.TenantRepository;
import com.sigeosrl.tileserver.utils.CQL;
import io.agroal.api.AgroalDataSource;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.ForbiddenException;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.springframework.cache.annotation.Cacheable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

@ApplicationScoped
@Slf4j
public class TileService {

    @ConfigProperty(name = "with-tenant")
    boolean withTenant;

    private final AgroalDataSource dataSource;

    private final TenantRepository tenantRepository;

    private final TenantDataSourceProvider tenantDataSourceProvider;


    public TileService(AgroalDataSource dataSource, TenantRepository tenantRepository,
                       TenantDataSourceProvider tenantDataSourceProvider) {
        this.dataSource = dataSource;
        this.tenantRepository = tenantRepository;
        this.tenantDataSourceProvider = tenantDataSourceProvider;
    }

    @Cacheable(value = "getTileSource")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public TileSource getTileSource(String tileName) throws SQLException {

        String sql = "select * from tiles.tile_source where name = ?";
        TileSource tileSource = null;
//        try (
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.setString(1, tileName);

        try (
                ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                tileSource = new TileSource()
                        .setName(resultSet.getString("name"))
                        .setDescription(resultSet.getString("description"))
                        .setFenced(resultSet.getBoolean("fenced"))
                        .setCtx(resultSet.getString("ctx") != null ? Set.of(resultSet.getString("ctx").split(",")) : null)
                        .setAllowedRoles(resultSet.getString("allowed_roles") != null ? Set.of(resultSet.getString("allowed_roles")
                                .replace("[", "")
                                .replace("]", "")
                                .replace(" " , "")
                                .replace("\"", "").split(",")) : null)
                        .setSchema(resultSet.getString("tile_schema"))
                        .setTable(resultSet.getString("tile_table"))
                        .setGeomColumn(resultSet.getString("tile_geom_column"))
                        .setProjInTransform(resultSet.getString("tile_proj_in_transform"))
                        .setSrid(resultSet.getInt("srid"))
                        .setFeatures(resultSet.getString("features"))
                        .setFilter(resultSet.getString("tile_filter"))
                        .setQuery(resultSet.getString("query"))
                        .setQueryFiltered(resultSet.getString("query_filtered"))
                        .setTenant(new TenantService(tenantRepository).getTenant(resultSet.getString("tenant_name")));
            }
        }

        log.debug("@@@@TileSource: {}", tileSource);
//
//        } catch (Exception e) {
//            log.error(e.getMessage());
//        }

        assert tileSource != null;
        return tileSource;
    }

    @Cacheable(value = "getFence")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public String getFence(Long fence) throws SQLException {

        String sql = "select st_asEwkt(geom) as geom from tiles.fence where id = ?";
        TileSource tileSource = null;
//        try (
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);

        statement.setLong(1, fence);

        try (
                ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return resultSet.getString("geom");
            }
        }

        return "";
    }

    //    @CacheResult(cacheName = "tile")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public byte[] getTile(String tileName, int z, int x, int y, int srid, String filter, Set<String> roles, Long fence) throws SQLException {

        TileSource tileSource = getTileSource(tileName);

        byte[] tile = null;
        String sql;

        if (fence != null && fence != 0 && tileSource.isFenced()) {
            sql = tileSource.getQueryFiltered();
        } else {
            sql = tileSource.getQuery();
        }

        sql = sql.replace("__FEATURES__", tileSource.getFeatures());
        sql = sql.replace("__GEOM__", tileSource.getGeomColumn());
        sql = sql.replace("__SRID__", String.valueOf(srid));
        sql = sql.replace("__SCHEMA__", tileSource.getSchema());
        sql = sql.replace("__TABLE__", tileSource.getTable());
        sql = sql.replace("__PROJ__", tileSource.getProjInTransform() != null && !tileSource.getProjInTransform().isEmpty() ? "'" + tileSource.getProjInTransform() + "', " : "");

        if (fence != null && fence != 0 && tileSource.isFenced()) {
            log.info("@@@@Fence: {}", fence);
            sql = sql.replace("__FENCE__", "ST_GeomFromEWKT(__FENCE__)");
            sql = sql.replace("__FENCE__", "'" + getFence(fence) + "'");
        } else {
            sql = sql.replace("__FENCE__", "");
        }

        // if tileSource has a filter, append it to the query
        if (tileSource.getFilter() != null && !tileSource.getFilter().isEmpty()) {
            log.debug("@@@@Filter: {}", tileSource.getFilter());
            sql = sql.replace("__FILTER_BE__", " AND " + tileSource.getFilter());
        } else {
            sql = sql.replace("__FILTER_BE__", "");
        }

        // if filter is not null and not empty, append it to the query
        if (filter != null && !filter.isEmpty()) {
            log.debug("@@@@Filter: {}", filter);
            try {
                sql = sql.replace("__FILTER_FE__", " AND " + CQL.cqlToSql(filter));
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        } else {
            sql = sql.replace("__FILTER_FE__", "");
        }

        boolean allowed = false;

        log.debug("@@@@Allowed roles: {}", tileSource.getAllowedRoles());
        log.debug("@@@@User roles: {}", roles);

        // if tileSource has allowedRoles, check that the user has one of them. if allowedRoles is null or empty, the tileSource is public. otherwise, forbidden.
        if (tileSource.getAllowedRoles() != null && !tileSource.getAllowedRoles().isEmpty()) {

            if (roles == null || roles.isEmpty()) {
                throw new ForbiddenException("Access denied");
            }

            allowed = roles.stream().anyMatch(role -> tileSource.getAllowedRoles().contains(role));

        } else {
            allowed = true;
        }

        log.debug("@@@@Allowed: {}", allowed);
        log.debug("@@@@QUERY: {}", sql);

        if (allowed) {
//            try (
            Connection connection;
            if (!withTenant) {
                connection = dataSource.getConnection();
            } else {
                connection = tenantDataSourceProvider.getConnection(tileSource.getTenant().getName());
            }
            PreparedStatement statement = connection.prepareStatement(sql);
//            ) {

//            // Here, calculate the bounding box using x, y, z
//            double[] bbox = Utils.calculateBoundingBox(x, y, z, srid);
//            for (int i = 1; i <= 8; i++) {
//                statement.setDouble(i, bbox[(i - 1) % 4]);
//            }


            statement.setInt(1, z);
            statement.setInt(2, x);
            statement.setInt(3, y);
            statement.setInt(4, z);
            statement.setInt(5, x);
            statement.setInt(6, y);

            log.debug("@@@@Statement: {}", statement);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    tile = resultSet.getBytes(1);

                }
            }

//            } catch (Exception e) {
//                log.error(e.getMessage());
//            }
        }

        log.debug("@@@@Tile: {}", tile);

        if (tile == null || tile.length == 0) {
            return new byte[0];
        } else {
            return tile;
        }
    }
}
