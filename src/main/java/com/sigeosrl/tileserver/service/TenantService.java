package com.sigeosrl.tileserver.service;

import com.sigeosrl.tileserver.model.Tenant;
import com.sigeosrl.tileserver.repository.TenantRepository;
import com.sigeosrl.tileserver.utils.Utils;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@ApplicationScoped
@Slf4j
public class TenantService {

    private final TenantRepository tenantRepository;

    @ConfigProperty(name = "key")
    String key;

    public TenantService(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    public Tenant createTenant(Tenant tenant) {
        try {
            tenant.setPassword(Utils.encrypt(tenant.getPassword(), key));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tenantRepository.save(tenant);
    }

    public Tenant updateTenant(String name, Tenant tenant) {

        Tenant existingTenant = tenantRepository.findById(name).orElseThrow(() -> new NotFoundException("Tenant not found"));

        existingTenant.setJdbcUrl(tenant.getJdbcUrl());
        existingTenant.setUsername(tenant.getUsername());
        try {
            existingTenant.setPassword(Utils.encrypt(tenant.getPassword(), key));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return tenantRepository.save(existingTenant);
    }

    public void deleteTenant(String name) {
        tenantRepository.deleteById(name);
    }

    public Tenant getTenant(String name) {
        return tenantRepository.findById(name).orElseThrow(() -> new NotFoundException("Tenant not found"));
    }

    @Cacheable(cacheNames = "tenants")
    public List<Tenant> getTenants() {
        return tenantRepository.findAll();
    }
}
