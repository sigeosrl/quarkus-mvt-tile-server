package com.sigeosrl.tileserver.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.repository.TileSourceCustomRepository;
import com.sigeosrl.tileserver.repository.TileSourceRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.NotFoundException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;

@ApplicationScoped
@Slf4j
public class TileSourceService {

    private final TileSourceRepository tileSourceRepo;
    private final TileSourceCustomRepository tileSourceRepository;
    private final TenantService tenantService;

    public TileSourceService(TileSourceRepository tileSourceRepo, TileSourceCustomRepository tileSourceRepository, TenantService tenantService) {
        this.tileSourceRepo = tileSourceRepo;
        this.tileSourceRepository = tileSourceRepository;
        this.tenantService = tenantService;
    }

    public TileSource createTileSource(TileSource tileSource) {

        try {
            tenantService.getTenant(tileSource.getTenant().getName());
        } catch (NotFoundException e) {
            tenantService.createTenant(tileSource.getTenant());
        }

        return tileSourceRepo.save(tileSource);
    }

    public TileSource updateTileSource(String name, TileSource tileSource, Set<String> roles) throws JsonProcessingException {

        TileSource existingTileSource = tileSourceRepository.findByNameAndAllowedRolesIn(name, roles).orElseThrow(() -> new NotFoundException("TileSource not found"));

        try {
            existingTileSource.setTenant(tenantService.getTenant(tileSource.getTenant().getName()));
        } catch (NotFoundException e) {
            existingTileSource.setTenant(tenantService.createTenant(tileSource.getTenant()));
        }

        existingTileSource
                .setDescription(tileSource.getDescription())
                .setCtx(tileSource.getCtx())
                .setAllowedRoles(tileSource.getAllowedRoles())
                .setSchema(tileSource.getSchema())
                .setTable(tileSource.getTable())
                .setGeomColumn(tileSource.getGeomColumn())
                .setProjInTransform(tileSource.getProjInTransform())
                .setSrid(tileSource.getSrid())
                .setFeatures(tileSource.getFeatures())
                .setFilter(tileSource.getFilter())
                .setFenced(tileSource.isFenced())
                .setQuery(tileSource.getQuery())
                .setQueryFiltered(tileSource.getQueryFiltered())
        ;

        return tileSourceRepo.save(existingTileSource);
    }

    public void deleteTileSource(String name, Set<String> roles) {

        TileSource tileToDelete = tileSourceRepo.findById(name).orElseThrow(() -> new NotFoundException("TileSource not found"));

        if (tileToDelete.getAllowedRoles() == null || tileToDelete.getAllowedRoles().isEmpty()) {
            tileSourceRepo.delete(tileToDelete);
            return;
        }

        if (tileToDelete.getAllowedRoles().stream().anyMatch(roles::contains)) {
            tileSourceRepo.delete(tileToDelete);
            return;
        }

        throw new ForbiddenException("Access denied");
    }

    public TileSource getTileSource(String name, Set<String> roles) throws JsonProcessingException {
        return tileSourceRepository.findByNameAndAllowedRolesIn(name, roles).orElseThrow(() -> new NotFoundException("TileSource not found"));
    }

    public List<TileSource> getTileSources(Set<String> roles) throws JsonProcessingException {
        return tileSourceRepository.findByAllowedRolesIn(roles);
    }
}
