package com.sigeosrl.tileserver.repository;

import com.sigeosrl.tileserver.model.TileSource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TileSourceRepository extends JpaRepository<TileSource, String> {

}
