package com.sigeosrl.tileserver.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sigeosrl.tileserver.model.Tenant;
import com.sigeosrl.tileserver.model.TileSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class TileSourceCustomRepositoryImpl implements TileSourceCustomRepository {

    @Autowired
    private DataSource dataSource;

    private TileSource mapResultSetToTileSource(ResultSet rs) throws SQLException {
        TileSource tileSource = new TileSource();

        // Standard String columns
        tileSource.setName(rs.getString("name"));
        tileSource.setDescription(rs.getString("description"));
        tileSource.setSchema(rs.getString("tile_schema"));
        tileSource.setTable(rs.getString("tile_table"));
        tileSource.setGeomColumn(rs.getString("tile_geom_column"));
        tileSource.setFilter(rs.getString("tile_filter"));
        tileSource.setFeatures(rs.getString("features"));

        // Integer columns
        tileSource.setSrid(rs.getInt("srid"));

        // JSONB columns to Set<String>
        String ctxJson = rs.getString("ctx");
        String allowedRolesJson = rs.getString("allowed_roles");

        // Use Jackson to convert JSON string to Set<String>
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Set<String> ctx = objectMapper.readValue(ctxJson, new TypeReference<Set<String>>() {});
            Set<String> allowedRoles = objectMapper.readValue(allowedRolesJson, new TypeReference<Set<String>>() {});

            tileSource.setCtx(ctx);
            tileSource.setAllowedRoles(allowedRoles);
        } catch (IOException e) {
            throw new SQLException("Failed to parse JSONB column", e);
        }

        // Assuming Tenant is another entity and only the ID (or reference) is fetched
        // In this example, let's say the tenant_id is the reference fetched
        Tenant tenant = new Tenant();
        tenant.setName(rs.getString("tenant_name"));
        tileSource.setTenant(tenant);

        return tileSource;
    }

    @Override
    public List<TileSource> findByAllowedRolesIn(Set<String> roles) throws JsonProcessingException {

        roles = roles.stream()
                .filter(role -> role.equals(role.toUpperCase()))
                .collect(Collectors.toSet());

        String rolesJson = new ObjectMapper().writeValueAsString(roles);
        String sql = "SELECT * FROM tiles.tile_source WHERE allowed_roles @> ?::jsonb";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)) {

            log.debug("@@@@ findByAllowedRolesIn: roles: {}", rolesJson);

            stmt.setString(1, rolesJson);

            ResultSet rs = stmt.executeQuery();

            List<TileSource> tileSources = new ArrayList<>();
            while (rs.next()) {
                tileSources.add(mapResultSetToTileSource(rs));
            }
            return tileSources;
        } catch (SQLException e) {
            throw new RuntimeException("Failed to fetch TileSource by roles", e);
        }
    }

    @Override
    public Optional<TileSource> findByNameAndAllowedRolesIn(String name, Set<String> roles) throws JsonProcessingException {

        roles = roles.stream()
                .filter(role -> role.equals(role.toUpperCase()))
                .collect(Collectors.toSet());

        String rolesJson = new ObjectMapper().writeValueAsString(roles);
        String sql = "SELECT * FROM tiles.tile_source WHERE name = ? AND allowed_roles @> ?::jsonb";


        try (Connection conn = dataSource.getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)) {

            log.debug("@@@@ findByNameAndAllowedRolesIn: name: {}, roles: {}", name, rolesJson);

            stmt.setString(1, name);
            stmt.setString(2, rolesJson);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(mapResultSetToTileSource(rs));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to fetch TileSource by name and roles", e);
        }
    }
}

