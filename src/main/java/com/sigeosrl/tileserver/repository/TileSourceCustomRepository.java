package com.sigeosrl.tileserver.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sigeosrl.tileserver.model.TileSource;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TileSourceCustomRepository {

    List<TileSource> findByAllowedRolesIn(Set<String> roles) throws JsonProcessingException;

    Optional<TileSource> findByNameAndAllowedRolesIn(String name, Set<String> roles) throws JsonProcessingException;
}
