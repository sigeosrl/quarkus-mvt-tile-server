package com.sigeosrl.tileserver.config.customtoken;

import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.model.Token;
import com.sigeosrl.tileserver.service.TileService;
import com.sigeosrl.tileserver.service.TokenService;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@ApplicationScoped
@Provider
@Priority(1) // Set a priority to ensure this filter runs before Keycloak's filter (which typically has a higher priority)
@Slf4j
public class BypassKeycloakFilter implements ContainerRequestFilter {

    @Inject
    TokenService tokenService;
    @Inject
    TileService tileService;

    @Override
    public void filter(ContainerRequestContext requestContext) {

        // Check if the "token" query parameter is present in the request
        UriInfo uriInfo = requestContext.getUriInfo();
        List<String> tokens = uriInfo.getQueryParameters().get("token");
        List<String> fence = uriInfo.getQueryParameters().get("fence");
        Long fenceId;
        if (fence != null && !fence.isEmpty()) {
            fenceId = Long.parseLong(fence.get(0));
        } else {
            fenceId = null;
        }

        if (tokens != null && !tokens.isEmpty()) {
            String token = tokens.get(0); // Assuming you only care about the first "token" query parameter

            // Check if the token is valid or meets your criteria for bypassing Keycloak
            if (shouldBypassKeycloak(token)) {
                // If the condition is met, you can return a response directly or handle the request as needed.
                // For example, return a 200 OK response to bypass authentication:

                // Extract z, x, y, srid, and filter from the request contex
                String tileName = uriInfo.getPathParameters().getFirst("tile");
                int z = extractIntPathParam(uriInfo, "z");
                int x = extractIntPathParam(uriInfo, "x");
                int y = extractIntPathParam(uriInfo, "y");
                int srid = extractIntQueryParam(uriInfo, "srid", 3857); // Default value 3857
                String filter = uriInfo.getQueryParameters().getFirst("filter");

                // Call the getTile method from TileService using the extracted values
                try {

                    // retrieve token from DB
                    Token tokenRetrievd = tokenService.getToken(UUID.fromString(token));

                    if (tokenRetrievd.getTileSourceSet().stream().map(TileSource::getName).toList().contains(tileName)) {
                        log.debug("token {} is valid for tile {}", token, tileName);
                        if (fenceId != null && fenceId != 0) {
                            log.debug("fenceId: {}", fenceId);
                        }
                        byte[] tile = tileService.getTile(tileName, z, x, y, srid, filter, Set.of("ADMIN"), fenceId);
                        handleCors(requestContext, tile);
                    }
                } catch (SQLException e) {
                    log.error("Error getting tile", e);
                }
            }
        }

        // If the token is not present or doesn't meet the bypass condition, let Keycloak handle authentication.
    }

    private int extractIntQueryParam(UriInfo uriInfo, String paramName, int defaultValue) {
        String paramValue = uriInfo.getQueryParameters().getFirst(paramName);
        if (paramValue != null) {
            try {
                return Integer.parseInt(paramValue);
            } catch (NumberFormatException e) {
                // Handle parsing error if needed
            }
        }
        return defaultValue;
    }

    private int extractIntPathParam(UriInfo uriInfo, String paramName) {
        // Use @PathParam to extract the integer parameter from the path
        try {
            return Integer.parseInt(uriInfo.getPathParameters().getFirst(paramName));
        } catch (NumberFormatException e) {
            // Handle parsing error if needed
            return 0; // Default value
        }
    }

    public boolean shouldBypassKeycloak(String token) {
        // Check if the token is present in TokenService
        return tokenService.isValid(UUID.fromString(token));
    }

    // This method handles CORS preflight requests
    private void handleCors(ContainerRequestContext requestContext, byte[] tile) {
        // Add CORS headers to allow requests from any origin
        requestContext.abortWith(Response.ok(tile).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .build());
    }

}