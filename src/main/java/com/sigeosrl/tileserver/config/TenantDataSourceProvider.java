package com.sigeosrl.tileserver.config;

import com.sigeosrl.tileserver.model.Tenant;
import com.sigeosrl.tileserver.service.TenantService;
import com.sigeosrl.tileserver.utils.Utils;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ApplicationScoped
@Slf4j
public class TenantDataSourceProvider {

    private final TenantService tenantService;
    private final Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    @ConfigProperty(name = "key")
    String key;

    public TenantDataSourceProvider(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    public Connection getConnection(String tenantId) {
        return connectionMap.computeIfAbsent(tenantId, id -> {
            try {
                Tenant tenant = tenantService.getTenant(id);
                if (tenant == null) {
                    throw new SQLException("Tenant information not found");
                }
                Connection connection = DriverManager.getConnection(
                        tenant.getJdbcUrl(),
                        tenant.getUsername(),
                        Utils.decrypt(tenant.getPassword(), key)
                );

                // Make the connection read-only
                connection.setReadOnly(true);

                return connection;
            } catch (Exception e) {
                log.error("Error getting connection for tenantId {}: {}", id, e.getMessage(), e);
                throw new RuntimeException(e);
            }
        });
    }
}