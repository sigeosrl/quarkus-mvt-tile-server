package com.sigeosrl.tileserver.init;

import io.quarkus.arc.profile.IfBuildProfile;
import io.quarkus.runtime.StartupEvent;
import jakarta.annotation.Priority;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.nio.file.Files;
import java.nio.file.Paths;

@Singleton
@IfBuildProfile("init")
public class DatabaseInitializer {

    @Inject
    EntityManager em;

    @ConfigProperty(name = "database.init-file")
    String initFile;

    @Transactional
    void onStart(@Observes @Priority(value = 10) StartupEvent ev) throws Exception {
        String sql = new String(Files.readAllBytes(Paths.get(initFile)));
        em.createNativeQuery(sql).executeUpdate();
    }
}