package com.sigeosrl.tileserver.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "tile_source")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@ToString
public class TileSource implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(columnDefinition = "TEXT")
    private String query;

    @Column(name = "query_filtered", columnDefinition = "TEXT")
    private String queryFiltered;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "ctx", columnDefinition = "jsonb")
    private Set<String> ctx;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "allowed_roles", columnDefinition = "jsonb")
    private Set<String> allowedRoles;

    @ManyToOne
    private Tenant tenant;

    @Column(name = "tile_schema")
    private String schema;

    @Column(name = "tile_table")
    private String table;

    @Column(name = "tile_geom_column")
    private String geomColumn;

    @Column(name = "tile_proj_in_transform")
    private String projInTransform;

    private int srid;

    private String features;

    @Column(name = "tile_filter", columnDefinition = "TEXT")
    private String filter;

    private boolean fenced = false;
}
