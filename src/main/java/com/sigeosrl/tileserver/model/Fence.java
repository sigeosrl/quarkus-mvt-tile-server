package com.sigeosrl.tileserver.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnTransformer;

@Entity
@Table(name = "fence")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Fence {

    @Id
    private Long id;
    private String name;
    private String type;

    @ColumnTransformer(
            read = "ST_AsEWKT(geom)",
            write = "ST_GeomFromEWKT(?)"
    )
    @Column(columnDefinition = "geometry")
    private String geom;
}
