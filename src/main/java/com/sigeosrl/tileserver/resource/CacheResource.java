package com.sigeosrl.tileserver.resource;

import com.sigeosrl.tileserver.resource.log.Log;
import io.quarkus.cache.CacheInvalidate;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Resource;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;

import java.time.Instant;


@Resource
@Path("/cache")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
//@Authenticated // TODO: uncomment this line to enable authentication
public class CacheResource {

    @Inject
    SecurityIdentity securityIdentity;

    @POST
    @Path("/clear/tile")
    @CacheInvalidate(cacheName = "tile")
    @Operation(summary = "Clear tile cache", description = "Clear tile cache")
    public void clearCache() {

        log();
    }

    @POST
    @Path("/clear/tenants")
    @CacheInvalidate(cacheName = "tenants")
    @Operation(summary = "Clear tenants cache", description = "Clear tenants cache")
    public void clearTenantsCache() {

        log();
    }

    private void log() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, securityIdentity.getPrincipal().getName());
        // do nothing
        Log.logEnd(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
    }
}
