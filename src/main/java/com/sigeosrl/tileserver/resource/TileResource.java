package com.sigeosrl.tileserver.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.resource.log.Log;
import com.sigeosrl.tileserver.service.TileService;
import com.sigeosrl.tileserver.service.TileSourceService;
import io.quarkus.cache.CacheInvalidate;
import io.quarkus.cache.CacheResult;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Resource;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;

import java.sql.SQLException;
import java.time.Instant;
import java.util.Set;

@Resource
@Path("/tiles")
@Slf4j
@Authenticated // TODO: uncomment this line to enable authentication
public class TileResource {

    @Inject
    SecurityIdentity identity;

    @Inject
    TileSourceService tileSourceService;

    @Inject
    TileService tileService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get tile sources", description = "Get tile sources")
    public Response getTileSources() {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, identity.getPrincipal().getName());
        Set<String> roles = identity.getRoles();

        log.debug("@@@@ getTileSources: roles: {}", roles);

        try {
            Response response = Response.ok(tileSourceService.getTileSources(roles)).build();
            Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (Exception e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{tile}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get tile source", description = "Get tile source")
    public Response getTileSource(@PathParam("tile") String tileName) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, identity.getPrincipal().getName());

        Set<String> roles = identity.getRoles();

        try {
            Response response = Response.ok(tileSourceService.getTileSource(tileName, roles)).build();
            Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (NotFoundException e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create tile source", description = "Create tile source")
    public Response createTileSource(TileSource tileSource) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, identity.getPrincipal().getName());

        Set<String> roles = identity.getRoles();

        if (tileSource.getAllowedRoles() == null || tileSource.getAllowedRoles().isEmpty()) {
            tileSource.setAllowedRoles(roles);
        }

        try {
            Response response = Response.ok(tileSourceService.createTileSource(tileSource)).build();
            Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (Exception e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/{tile}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update tile source", description = "Update tile source")
    public Response updateTileSource(@PathParam("tile") String tileName, TileSource tileSource) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, identity.getPrincipal().getName());
        Set<String> roles = identity.getRoles();

        try {
            Response response = Response.ok(tileSourceService.updateTileSource(tileName, tileSource, roles)).build();
            Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (NotFoundException e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("/{tile}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Delete tile source", description = "Delete tile source")
    public Response deleteTileSource(@PathParam("tile") String tileName) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, identity.getPrincipal().getName());
        Set<String> roles = identity.getRoles();

        try {
            tileSourceService.deleteTileSource(tileName, roles);
            Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return Response.ok().build();
        } catch (NotFoundException e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{tile}/{z}/{x}/{y}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation(summary = "Get tile", description = "Get tile")
//    @CacheResult(cacheName = "tiles")
    public Response tile(
            @PathParam("tile") String tileName,
            @PathParam("z") int z,
            @PathParam("x") int x,
            @PathParam("y") int y,
            @QueryParam("srid") int srid,
            @QueryParam("filter") String filter) throws SQLException {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, identity.getPrincipal().getName());

        if (srid == 0) {
            srid = 3857;
            log.debug("srid is not specified, using default value: {}", srid);
        }

        Set<String> roles = identity.getRoles();
        Long fence = identity.getAttribute("fence");  // 0 = no fence, num = fence

        try {
            byte[] tile = tileService.getTile(tileName, z, x, y, srid, filter, roles, fence);
            Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return Response.ok(tile).build();
        } catch (NotFoundException e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @CacheInvalidate(cacheName = "tiles")
    @Path("/invalidate")
    @POST
    @Operation(summary = "Invalidate tile cache", description = "Invalidate tile cache")
    public Response invalidateCache() {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();
        Instant start = Log.logStart(method, identity.getPrincipal().getName());
        Log.logEnd(method, identity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
        return Response.ok("Cache invalidated").build();
    }
}
