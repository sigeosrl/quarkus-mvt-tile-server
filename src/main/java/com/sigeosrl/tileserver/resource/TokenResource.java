package com.sigeosrl.tileserver.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sigeosrl.tileserver.dto.RequestDTO;
import com.sigeosrl.tileserver.model.TileSource;
import com.sigeosrl.tileserver.service.TileSourceService;
import com.sigeosrl.tileserver.service.TokenService;
import io.quarkus.security.Authenticated;
import jakarta.annotation.Resource;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Resource
@Path("/tokens")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Authenticated
@Slf4j
public class TokenResource {

    @Inject
    TokenService tokenService;

    @Inject
    TileSourceService tileSourceService;

    @GET
    @RolesAllowed("ADMIN")
    public Response getTokens() {
        return Response.ok(tokenService.getTokens()).build();
    }

    @GET
    @Path("/{token}")
    @RolesAllowed("ADMIN")
    @Operation(summary = "Get token", description = "Get token")
    public Response getToken(@PathParam("token") String token) {
        return Response.ok(tokenService.getToken(UUID.fromString(token))).build();
    }

    @POST
    @RolesAllowed("ADMIN")
    @Operation(summary = "Create token", description = "Create token")
    public Response createToken(RequestDTO request) {
        ZonedDateTime expiration = request.getExpiration();
        Set<TileSource> tileSourceSet = request.getTileSources().stream().map(ts -> {
            TileSource tileSource = null;
            try {
                tileSource = tileSourceService.getTileSource(ts, Set.of("ADMIN"));
            } catch (JsonProcessingException e) {
                log.error("Error getting tile source", e);
            }
            return tileSource;
        }).collect(Collectors.toSet());
        return Response.ok(tokenService.createToken(tileSourceSet, expiration)).build();
    }

    @DELETE
    @Path("/{token}")
    @RolesAllowed("ADMIN")
    @Operation(summary = "Delete token", description = "Delete token")
    public Response deleteToken(@PathParam("token") String token) {
        tokenService.deleteToken(UUID.fromString(token));
        return Response.ok().build();
    }
}
