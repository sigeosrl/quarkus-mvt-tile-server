package com.sigeosrl.tileserver.resource;

import com.sigeosrl.tileserver.model.Tenant;
import com.sigeosrl.tileserver.resource.log.Log;
import com.sigeosrl.tileserver.service.TenantService;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Resource;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;

import java.time.Instant;

@Resource
@Path("/tenants")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
//@Authenticated // TODO: uncomment this line to enable authentication
public class TenantResource {

    @Inject
    SecurityIdentity securityIdentity;
    @Inject
    TenantService tenantService;


    @GET
    public Response getTenants() {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, securityIdentity.getPrincipal().getName());

        try {
            Response response = Response.ok(tenantService.getTenants()).build();
            Log.logEnd(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (Exception e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{tenant}")
    @Operation(summary = "Get tenant", description = "Get tenant")
    public Response getTenant(@PathParam("tenant") String tenantName) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, securityIdentity.getPrincipal().getName());

        try {
            Response response = Response.ok(tenantService.getTenant(tenantName)).build();
            Log.logEnd(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (NotFoundException e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @POST
    @Operation(summary = "Create tenant", description = "Create tenant")
    public Response createTenant(Tenant tenant) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, securityIdentity.getPrincipal().getName());

        try {
            Response response = Response.ok(tenantService.createTenant(tenant)).build();
            Log.logEnd(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (Exception e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/{tenant}")
    @Operation(summary = "Update tenant", description = "Update tenant")
    public Response updateTenant(@PathParam("tenant") String tenantName, Tenant tenant) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, securityIdentity.getPrincipal().getName());

        try {
            Response response = Response.ok(tenantService.updateTenant(tenantName, tenant)).build();
            Log.logEnd(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return response;
        } catch (NotFoundException e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("/{tenant}")
    @Operation(summary = "Delete tenant", description = "Delete tenant")
    public Response deleteTenant(@PathParam("tenant") String tenantName) {

        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String method = stackTrace[1].getMethodName();

        Instant start = Log.logStart(method, securityIdentity.getPrincipal().getName());

        try {
            tenantService.deleteTenant(tenantName);
            Log.logEnd(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.OK), start);
            return Response.ok().build();
        } catch (NotFoundException e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.NOT_FOUND), start, e);
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            Log.logException(method, securityIdentity.getPrincipal().getName(), String.valueOf(Response.Status.INTERNAL_SERVER_ERROR), start, e);
            return Response.serverError().build();
        }
    }
}
