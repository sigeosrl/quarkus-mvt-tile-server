package com.sigeosrl.tileserver.resource.log;

import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class Log {

    private Log() {
    }

    public static final String PATTERN = "yyyy-MM-dd HH:mm:ss:SSS";

    public static Instant logStart(String method, String username) {
        Instant start = Instant.now();
        ZonedDateTime zdtStart = start.atZone(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN);
        String formattedStart = zdtStart.format(formatter);
        log.info("Method \"{}\" called by: {}, started at: {}", method, username, formattedStart);
        return start;
    }

    public static void logEnd(String method, String username, String status, Instant start) {
        Instant end = Instant.now();
        ZonedDateTime zdtEnd = end.atZone(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN);
        String formattedEnd = zdtEnd.format(formatter);
        log.info("Method \"{}\" called by: {}, ended at: {}, status: {}, elapsed time: {} ms", method, username, formattedEnd, status, end.toEpochMilli() - start.toEpochMilli());
    }

    public static void logException(String method, String username, String status, Instant start, Throwable e) {
        Instant end = Instant.now();
        ZonedDateTime zdtEnd = end.atZone(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN);
        String formattedEnd = zdtEnd.format(formatter);
        log.error("Method \"{}\" called by: {}, ended at: {}, status: {}, elapsed time: {} ms, exception: >> {}", method, username, formattedEnd, status, end.toEpochMilli() - start.toEpochMilli(), e.getMessage());
    }
}

