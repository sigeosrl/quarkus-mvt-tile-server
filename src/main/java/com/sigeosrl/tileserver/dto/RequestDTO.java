package com.sigeosrl.tileserver.dto;


import jakarta.json.bind.annotation.JsonbDateFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.ZonedDateTime;
import java.util.Set;

@Data
@Accessors(chain = true)
public class RequestDTO {

    private Set<String> tileSources;

    @JsonbDateFormat(value = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expiration;
}
