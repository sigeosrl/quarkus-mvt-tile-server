package com.sigeosrl.tileserver.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.util.HashSet;
import java.util.Set;

@Slf4j
public class SecurityUtils {

    public static Set<String> getRolesFromClient(String clientForRoles, JsonWebToken jwt) {

        Set<String> rs = new HashSet<>();
        log.debug("@@@Claims: {}",jwt.getClaimNames());

        // Create ObjectMapper
        ObjectMapper objectMapper = new ObjectMapper();

        // Parse JSON string
        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readTree(jwt.getClaim("resource_access").toString());
        } catch (JsonProcessingException e) {
            log.error("Error parsing JSON: {}", e.getMessage());
        }

        // Access elements in the JSON structure
        JsonNode cfaNode = jsonNode.get(clientForRoles);
        JsonNode rolesNode = cfaNode.get("roles");

        // Iterate over roles
        if (rolesNode.isArray()) {
            for (JsonNode role : rolesNode) {
                log.debug("@@@Role: {}", role.asText());
                rs.add(role.asText());
            }
        }

        log.debug("@@@RS: {}", rs);

        return rs;
    }

    public static Set<String> getRolesFromRealm(JsonWebToken jwt) {

        Set<String> rs = new HashSet<>();
        log.debug("@@@Claims: {}",jwt.getClaimNames());

        // Create ObjectMapper
        ObjectMapper objectMapper = new ObjectMapper();


        // get roles from realm
        Set<String> realmRoles = jwt.getClaim("realm_access");
        if (realmRoles != null) {
            JsonNode realmRolesNode = null;
            try {
                realmRolesNode = objectMapper.readTree(realmRoles.toString());
            } catch (JsonProcessingException e) {
                log.error("Error parsing JSON: {}", e.getMessage());
            }
            JsonNode rolesNodeRealm = realmRolesNode.get("roles");
            if (rolesNodeRealm.isArray()) {
                for (JsonNode role : rolesNodeRealm) {
                    log.debug("@@@Role: {}", role.asText());
                    rs.add(role.asText());
                }
            }
        }

        log.debug("@@@RS: {}", rs);

        return rs;
    }
}
