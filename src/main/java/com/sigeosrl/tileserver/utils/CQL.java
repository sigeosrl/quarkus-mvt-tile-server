package com.sigeosrl.tileserver.utils;

import org.geotools.filter.text.cql2.CQLException;
import org.geotools.filter.text.ecql.ECQL;

public class CQL {

    private CQL() {
        // Utility class
    }

    public static boolean isValidCqlFilter(String cqlFilter) throws CQLException {
        return ECQL.toFilter(cqlFilter) != null;
    }

    public static String cqlToSql(String cqlFilter) throws CQLException {

        // validate CQL syntax for security reasons
        if (cqlFilter == null || cqlFilter.isEmpty()) {
            return "";
        }

        if (isValidCqlFilter(cqlFilter)) {

            String sqlWhere = cqlFilter;

            // Replacing CQL specific syntax with SQL syntax
            sqlWhere = sqlWhere.replace(" eq ", " = ");
            sqlWhere = sqlWhere.replace(" ne ", " != ");
            sqlWhere = sqlWhere.replace(" gt ", " > ");
            sqlWhere = sqlWhere.replace(" lt ", " < ");
            sqlWhere = sqlWhere.replace(" ge ", " >= ");
            sqlWhere = sqlWhere.replace(" le ", " <= ");

            // convert list to array
            sqlWhere = sqlWhere.replace("(", "ARRAY[");
            sqlWhere = sqlWhere.replace(")", "]");

            return sqlWhere;
        }

        return "";
    }
}
