package com.sigeosrl.tileserver.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Set;

public class Utils {

    private Utils() {
        // Utility class
    }

    private final static String iv = "asdg251f3asdg251";

    public static double[] calculateBoundingBox(int x, int y, int zoom, int srid) {

        double[] bbox = new double[]{0, 0, 0, 0};

        if ("EPSG:4326".equals("EPSG:" + srid)) {
            // Calculate the number of tiles at this zoom level
            int numTiles = (int) Math.pow(2, zoom);

            // Calculate the geographical bounds of the tile
            double lon_deg_per_tile = 360.0 / numTiles;
            double lat_deg_per_tile = 180.0 / numTiles;

            double minLon = -180 + (x * lon_deg_per_tile);
            double maxLon = minLon + lon_deg_per_tile;

            double maxLat = 90 - (y * lat_deg_per_tile);
            double minLat = maxLat - lat_deg_per_tile;

            bbox = new double[]{minLon, minLat, maxLon, maxLat};
        }

        if ("EPSG:3857".equals("EPSG:" + srid)) {
            int numTiles = (int) Math.pow(2, zoom);

            // Earth's circumference in meters at the equator.
            double originalExtent = 2 * Math.PI * 6378137;

            // Tile size in meters
            double tileWidth = originalExtent / numTiles;

            // Calculate the geographical bounds of the tile
            double minLon = x * tileWidth - originalExtent / 2.0;
            double minLat = originalExtent / 2.0 - (y + 1) * tileWidth;
            double maxLon = (x + 1) * tileWidth - originalExtent / 2.0;
            double maxLat = originalExtent / 2.0 - y * tileWidth;

            bbox = new double[]{minLon, minLat, maxLon, maxLat};
        }

        return bbox;
    }

    public static IvParameterSpec generateIv() {
        return new IvParameterSpec(iv.getBytes());
    }

    public static String encrypt(String data, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "AES");
        IvParameterSpec ivParameterSpec = generateIv();
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encrypted);
    }

    public static String decrypt(String encryptedData, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "AES");
        IvParameterSpec ivParameterSpec = generateIv();
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(encryptedData));
        return new String(decrypted);
    }

    public boolean setContainsString(Set<String> set, String str) {
        for (String s : set) {
            if (s.equals(str)) {
                return true;
            }
        }
        return false;
    }
}
