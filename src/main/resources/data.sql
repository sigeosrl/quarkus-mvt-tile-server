INSERT INTO tiles.tenant (jdbcurl, name, password, username)
VALUES ('jdbc:postgresql://localhost:25432/test-ts', 'tenant1', 'oxYyuDS4eBy19/U1GkQjgQ==', 'dev');

INSERT INTO tiles.tile_source (srid, description, name, tenant_name, tile_filter, tile_geom_column, tile_schema, tile_table, allowed_roles, ctx, features)
VALUES
    (3857, 'test', 'test1', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry1', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test2', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry2', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test3', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry3', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test4', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry4', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test5', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry5', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test6', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry6', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test7', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry7', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test8', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry8', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name'),
    (3857, 'test', 'test9', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry9', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name');
