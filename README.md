# QUARKUS MVT TILE SERVER by [SIGEO](https://sigeosrl.com)

![SIGEO LOGO](images/sigeo_chiaro.png)

# CONTRIBUTORS

![kambei](images/kambei.jpg)

[Andrea "Kambei" Bovi](https://kambei.dev)

## ENDPOINTS

### TENANTS

#### GET /tenants
Get all tenants

#### GET /tenants/:tenantName
Get a tenant by id

#### POST /tenants

Create a tenant

```json
{
    "name": "tenant1",
    "jdbcUrl": "jdbc:postgresql://localhost:5432/tenant1",
    "username": "tenant1",
    "password": "tenant1"
}
```

#### PUT /tenants/:tenantName

Update a tenant

```json
{
    "name": "tenant1",
    "jdbcUrl": "jdbc:postgresql://localhost:5432/tenant1",
    "username": "tenant1",
    "password": "tenant1"
}
```

#### DELETE /tenants/:tenantName

Delete a tenant

---

### TOKENS

#### GET /tokens/:token
Get a token by id

#### POST /tokens

Create a token
```json
{
    "tileSources": ["tile-source-1", "tile-source-2"],
    "expiration": "2021-12-31T23:59:59"
}
```

#### DELETE /tokens/:token
Delete a token

---

### TILES

#### GET /tiles
Get all tiles sources

#### GET /tiles/:tile
Get a tile source by id

#### POST /tiles
Create a tile source
```json
{
  "name": "exampleName",
  "description": "exampleDescription",
  "query": "exampleQuery",
  "queryFiltered": "exampleQueryFiltered",
  "ctx": ["context1", "context2"],
  "allowedRoles": ["role1", "role2"],
  "tenant": {
    "tenantDetail1": "value1",
    "tenantDetail2": "value2"
    // Tenant object details go here. Since it's a complex object, you would replace these placeholders with actual tenant information.
  },
  "schema": "exampleSchema",
  "table": "exampleTable",
  "geomColumn": "exampleGeomColumn",
  "projInTransform": "exampleProjInTransform",
  "srid": 4326,
  "features": "exampleFeatures",
  "filter": "exampleFilter",
  "fenced": false
}
```

Example Query:
```sql
SELECT ST_AsMVT(tile) FROM (
  SELECT
    __FEATURES__,
    ST_AsMVTGeom(
      __GEOM__,
      ST_TileEnvelope(?, ?, ?),
      4096,
      512,
      false
    ) as geom
  FROM __SCHEMA__.__TABLE__
    WHERE
    -- Bounding box condition
    __GEOM__ && ST_TileEnvelope(?, ?, ?)
    AND 1=1
    __FILTER_FE__
    __FILTER_BE__
) AS tile WHERE 1=1
```

Example Query Filtered:
```sql
SELECT ST_AsMVT(tile) FROM (
  SELECT
    __FEATURES__,
    ST_AsMVTGeom(
        __GEOM__,
        ST_TileEnvelope(?, ?, ?),
        4096,
        512,
        false
    ) as geom
  FROM __SCHEMA__.__TABLE__
    WHERE
    -- Bounding box condition
    __GEOM__ && ST_TileEnvelope(?, ?, ?)
    AND ST_Intersects(__GEOM__, __FENCE__)
    AND 1=1
    __FILTER_FE__
    __FILTER_BE__
) AS tile WHERE 1=1
```

#### PUT /tiles/:tile
Update a tile source
```json
{
  "name": "exampleName",
  "description": "exampleDescription",
  "query": "exampleQuery",
  "queryFiltered": "exampleQueryFiltered",
  "ctx": ["context1", "context2"],
  "allowedRoles": ["role1", "role2"],
  "tenant": {
    "tenantDetail1": "value1",
    "tenantDetail2": "value2"
    // Tenant object details go here. Since it's a complex object, you would replace these placeholders with actual tenant information.
  },
  "schema": "exampleSchema",
  "table": "exampleTable",
  "geomColumn": "exampleGeomColumn",
  "projInTransform": "exampleProjInTransform",
  "srid": 4326,
  "features": "exampleFeatures",
  "filter": "exampleFilter",
  "fenced": false
}
```

#### DELETE /tiles/:tile
Delete a tile source

### TILES MVT

#### GET /tiles/:tile/:z/:x/:y
Get a MVT tile


### GET /tiles/:tile/:z/:x/:y?token=TOKEN
Get a MVT tile with token bypassing Keycloak

---

## Launch the DBs and the Backend (and Locust for testing)

```bash
docker-compose up -d
```

or

```bash
docker-compose up -d --scale tile-server=3
```

Note: you should add `ts.test` to your `/etc/hosts` file pointing to `127.0.0.1`.



### Test with Locust

Go to http://localhost:8089/ and start the test.

