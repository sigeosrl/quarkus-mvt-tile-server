INSERT INTO tiles.tenant (jdbcurl, name, password, username)
VALUES ('jdbc:postgresql://localhost:5432/test-ts?currentSchema=tiles,util', 'tenant1', 'oxYyuDS4eBy19/U1GkQjgQ==', 'dev');

INSERT INTO tiles.tile_source (srid, description, name, tenant_name, tile_filter, tile_geom_column, tile_schema, tile_table, allowed_roles, ctx, features, fenced)
VALUES
    (3857, 'test', 'test2', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry2', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (3857, 'test', 'test1', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry1', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (3857, 'test', 'test3', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry3', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (4326, 'test', 'test4', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry4', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (4326, 'test', 'test5', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry5', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (4326, 'test', 'test6', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry6', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (32632, 'test', 'test7', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry7', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (32632, 'test', 'test8', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry8', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (32632, 'test', 'test9', 'tenant1', '1 = 1', 'geom', 'tiles', 'geometry9', '["ADMIN", "USER"]', '["CTX1", "CTX2"]', 'id, name', true),
    (32632, 'regioni', 'regioni', 'tenant1', '1 = 1', 'geom', 'tiles', 'regione', null, null, 'cod_reg as id, den_reg as deno', false);


INSERT INTO tiles.token (expiration, token)
VALUES
    ('2025-06-01T10:15:30.123+0100', '4d9781d5-c78d-4a08-b262-0c38dc17be82');


INSERT INTO tiles.token_tile_source(token_token, tilesourceset_name)
VALUES
    ('4d9781d5-c78d-4a08-b262-0c38dc17be82', 'regioni');


INSERT INTO tiles.fence (id, name) VALUES (1, 'test');