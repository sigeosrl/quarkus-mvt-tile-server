CREATE SCHEMA IF NOT EXISTS "tiles";

CREATE TABLE IF NOT EXISTS "tiles"."geometry1"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry1" USING GIST ("geom");


CREATE TABLE IF NOT EXISTS "tiles"."geometry2"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry2" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry3"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry3" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry4"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry4" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry5"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry5" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry6"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry6" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry7"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry7" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry8"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry8" USING GIST ("geom");

CREATE TABLE IF NOT EXISTS "tiles"."geometry9"
(
    id   SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL DEFAULT 'unnamed',
    geom geometry     NOT NULL
);
CREATE INDEX ON "tiles"."geometry9" USING GIST ("geom");



-- Function mostly copied from generate_random_point().
CREATE OR REPLACE FUNCTION generate_random_quasicentroid()
    RETURNS geometry AS
$func$
DECLARE
    -- Points are declared between -70/70 degrees on the Y-axis, and -180/180
    -- degrees on the X-axis, to allow room for creation of polygons and
    -- rendering on WMS (avoid cutoff in polar areas).
    x_min INTEGER := -180;
    x_max INTEGER := 180;
    y_min INTEGER := -70;
    y_max INTEGER := 70;
    srid INTEGER := 4326;
BEGIN
    RETURN (
        ST_SetSRID(
                ST_MakePoint(
                                random()*(x_max - x_min) + x_min,
                                random()*(y_max - y_min) + y_min
                    ),
                srid
            )
        );
END;
$func$
    LANGUAGE 'plpgsql' VOLATILE;

-- This function creates a polygon from a set of randomly defined angles, and
-- randomly defined distances from a quasicentroid along that angle.
CREATE OR REPLACE FUNCTION generate_random_polygon(
    quasicentroid geometry
)
    RETURNS geometry AS
$func$
DECLARE
    idx INTEGER;
    nodes geometry[];
    angle FLOAT;
    starting_angle FLOAT;
    distance FLOAT;
    max_distance FLOAT := 1;
BEGIN
    -- NOTE: Do NOT wrap parentheses around fraction! 'plpgsql' will not
    -- understand this query and will round down to 0.
    starting_angle := random()*1/3*PI();
    angle := starting_angle;
    -- Set a maximum of 20 vertices for a polygon
    FOR idx IN 1 .. 20 LOOP
            distance := random() * max_distance;
            SELECT array_append(
                           nodes,
                           ST_Translate(
                                   quasicentroid,
                                   sin(angle)*distance,
                                   cos(angle)*distance
                               )
                       ) INTO nodes;
            -- NOTE: Do NOT wrap parentheses around fraction! 'plpgsql' will not
            -- understand the query and will round down to 0.
            angle := angle + random()*2/3*PI();
            IF angle > 2 * PI() THEN EXIT; END IF;
        END LOOP;
    -- Close the polygon
    SELECT array_append(
                   nodes,
                   nodes[1]
               ) INTO nodes;
    RETURN ST_MakePolygon(
            ST_MakeLine(
                    nodes
                )
        );
END;
$func$
    LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION generate_random_polygons()
    RETURNS void AS
$func$
DECLARE
    num_records INTEGER := 1000;
    quasicentroid geometry;
BEGIN
    -- NOTE: Setting a seed to generate reproducible results.
    --SET seed TO 0.5;

    DROP TABLE IF EXISTS temp_postgis_polygons_random;
    -- Table name is 'temp_postgis_polygons_random'.
    -- Column name containing PostGIS data is called 'geom_polygons'.
    quasicentroid := generate_random_quasicentroid();
    CREATE TABLE temp_postgis_polygons_random (geom_polygons)
    AS SELECT generate_random_polygon(quasicentroid);

    -- Populate the table with number of records denoted in 'num_records'.
    FOR idx IN 1 .. num_records - 1 LOOP
            quasicentroid := generate_random_quasicentroid();
            INSERT INTO temp_postgis_polygons_random
            SELECT generate_random_polygon(quasicentroid);
        END LOOP;

    -- Create final table with column definitions.
    DROP TABLE IF EXISTS postgis_polygons_random;
    CREATE TABLE postgis_polygons_random (
                                             record_id SERIAL PRIMARY KEY,
                                             geom_polygons geometry
    );
    INSERT INTO postgis_polygons_random (geom_polygons)
    SELECT geom_polygons FROM temp_postgis_polygons_random;

    --DROP TABLE temp_postgis_polygons_random;
END;
$func$
    LANGUAGE 'plpgsql' VOLATILE;


-- SRID 3857 is used for the geometry columns
TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry1" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 3857)
FROM temp_postgis_polygons_random;


TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry2" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 3857)
FROM temp_postgis_polygons_random;

TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry3" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 3857)
FROM temp_postgis_polygons_random;



-- SRID 4326 is used for the geometry columns
TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry4" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 4326)
FROM temp_postgis_polygons_random;

TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry5" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 4326)
FROM temp_postgis_polygons_random;

TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry6" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 4326)
FROM temp_postgis_polygons_random;



-- SRID 32632 is used for the geometry columns
TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry7" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 32632)
FROM temp_postgis_polygons_random;

TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry8" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 32632)
FROM temp_postgis_polygons_random;

TRUNCATE TABLE temp_postgis_polygons_random;
SELECT generate_random_polygons();
INSERT INTO "tiles"."geometry9" ("name", "geom")
SELECT
    CONCAT('geometry1-', ROW_NUMBER() OVER (ORDER BY geom_polygons)),
    ST_Transform(geom_polygons, 32632)
FROM temp_postgis_polygons_random;



create table tiles.regione
(
    cod_reg    double precision not null
        primary key,
    cod_rip    double precision,
    den_reg    varchar(50),
    shape_leng numeric,
    shape_area numeric,
    geom       geometry
);

create index regione_geom_idx
    on tiles.regione using gist (geom);

INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (1, 1, 'Piemonte', 1333428.21141, 25386696869.8, 'SRID=32632;POLYGON((313279.2522 4879337.780099999,313279.2522 5145804.794600001,516960.2204999998 5145804.794600001,516960.2204999998 4879337.780099999,313279.2522 4879337.780099999))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (2, 1, 'Valle d''Aosta', 325836.368312, 3260854217.19, 'SRID=32632;POLYGON((329083.10089999996 5036573.5648,329083.10089999996 5093683.1566,417357.9578 5093683.1566,417357.9578 5036573.5648,329083.10089999996 5036573.5648))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (3, 1, 'Lombardia', 1511131.9049, 23863097424.7, 'SRID=32632;POLYGON((460624.4254999999 4947405.265000001,460624.4254999999 5165370.644300001,691489.7176999999 5165370.644300001,691489.7176999999 4947405.265000001,460624.4254999999 4947405.265000001))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (4, 2, 'Trentino-Alto Adige', 859873.139229, 13604721571.4, 'SRID=32632;POLYGON((605667.8839999996 5059559.332699999,605667.8839999996 5220292.2928,765973.4637000002 5220292.2928,765973.4637000002 5059559.332699999,605667.8839999996 5059559.332699999))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (5, 2, 'Veneto', 1140433.27885, 18345369036.2, 'SRID=32632;POLYGON((626924.4089000002 4965313.029100001,626924.4089000002 5175614.4954,819674.2566 5175614.4954,819674.2566 4965313.029100001,626924.4089000002 4965313.029100001))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (6, 2, 'Friuli Venezia Giulia', 769243.682097, 7932481707.34, 'SRID=32632;POLYGON((755892.9995999997 5058752.827199999,755892.9995999997 5172904.0736,883374.3613999998 5172904.0736,883374.3613999998 5058752.827199999,755892.9995999997 5058752.827199999))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (7, 1, 'Liguria', 1079642.12886, 5416152035.04, 'SRID=32632;POLYGON((379029.0533999996 4847812.5272,379029.0533999996 4947010.2686,585732.9929999998 4947010.2686,585732.9929999998 4847812.5272,379029.0533999996 4847812.5272))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (8, 2, 'Emilia-Romagna', 1289118.6153, 22501432636.7, 'SRID=32632;POLYGON((515693.1046000002 4847056.959899999,515693.1046000002 4998812.3695,801305.5542000001 4998812.3695,801305.5542000001 4847056.959899999,515693.1046000002 4847056.959899999))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (9, 3, 'Toscana', 1552065.13934, 22987437332.3, 'SRID=32632;POLYGON((554716.1145000001 4678321.384199999,554716.1145000001 4924771.4341,771618.3276000004 4924771.4341,771618.3276000004 4678321.384199999,554716.1145000001 4678321.384199999))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (10, 3, 'Umbria', 671416.043397, 8464223118.34, 'SRID=32632;POLYGON((736320.3827999998 4696229.102700001,736320.3827999998 4834848.0667,848666.6618999997 4834848.0667,848666.6618999997 4696229.102700001,736320.3827999998 4696229.102700001))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (11, 3, 'Marche', 702672.619865, 9344293316.7, 'SRID=32632;POLYGON((756931.8129000003 4735426.2741,756931.8129000003 4875352.454399999,901475.1542999996 4875352.454399999,901475.1542999996 4735426.2741,756931.8129000003 4735426.2741))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (12, 3, 'Lazio', 1201764.76412, 17231723416.4, 'SRID=32632;POLYGON((701604.1719000004 4524227.557700001,701604.1719000004 4746846.782500001,919531.4782999996 4746846.782500001,919531.4782999996 4524227.557700001,701604.1719000004 4524227.557700001))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (13, 4, 'Abruzzo', 664538.009079, 10831496151, 'SRID=32632;POLYGON((832439 4626661.619999999,832439 4760850.0231,978766.4670000002 4760850.0231,978766.4670000002 4626661.619999999,832439 4626661.619999999))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (14, 4, 'Molise', 471197.627153, 4460436571.7, 'SRID=32632;POLYGON((911255.4467000002 4594279.3862,911255.4467000002 4673741.1479,1012712.1359000001 4673741.1479,1012712.1359000001 4594279.3862,911255.4467000002 4594279.3862))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (15, 4, 'Campania', 1090752.84229, 13670596992, 'SRID=32632;POLYGON((899211.5060999999 4446490.024,899211.5060999999 4610208.6369,1078993.7199 4610208.6369,1078993.7199 4446490.024,899211.5060999999 4446490.024))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (16, 4, 'Puglia', 1507342.40524, 19540517723.4, 'SRID=32632;POLYGON((994467.1486999998 4446414.521600001,994467.1486999998 4697000.0922,1312016.1509999996 4697000.0922,1312016.1509999996 4446414.521600001,994467.1486999998 4446414.521600001))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (17, 4, 'Basilicata', 675750.709571, 10073110429.5, 'SRID=32632;POLYGON((1034266.4748 4439934.035800001,1034266.4748 4577109.819800001,1167941.7023999998 4577109.819800001,1167941.7023999998 4439934.035800001,1034266.4748 4439934.035800001))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (18, 4, 'Calabria', 934657.548755, 15221614065.2, 'SRID=32632;POLYGON((1077472.9214000003 4218138.5702,1077472.9214000003 4470368.209100001,1210848.9228999997 4470368.209100001,1210848.9228999997 4218138.5702,1077472.9214000003 4218138.5702))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (19, 5, 'Sicilia', 1743759.26923, 25832545368.8, 'SRID=32632;POLYGON((760976.2416000003 3933682.9321999997,760976.2416000003 4314351.6664,1082290.5195000004 4314351.6664,1082290.5195000004 3933682.9321999997,760976.2416000003 3933682.9321999997))');
INSERT INTO tiles.regione(cod_reg, cod_rip, den_reg, shape_leng, shape_area, geom) VALUES (20, 5, 'Sardegna', 2129142.61196, 24099452848.5, 'SRID=32632;POLYGON((426598.04119999986 4301317.255000001,426598.04119999986 4573605.3544,570166.0334000001 4573605.3544,570166.0334000001 4301317.255000001,426598.04119999986 4301317.255000001))');
